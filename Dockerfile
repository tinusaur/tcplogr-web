# Dockerfile for PHP

FROM php:7.4-fpm-alpine
RUN docker-php-ext-install pcntl
RUN docker-php-ext-install sockets

RUN apk add --no-cache --virtual .build-deps $PHPIZE_DEPS \
	&& pecl install redis \
	&& docker-php-ext-enable redis \
	&& apk del .build-deps

RUN mkdir /var/log/tcplogr

# ---- Install utils (optional) ----
# RUN apk add bash

# ---- Startup ----
COPY ./run.sh /srv/tcplogr/
RUN chmod +x /srv/tcplogr/run.sh
CMD [ "/srv/tcplogr/run.sh" ]

