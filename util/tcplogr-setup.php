#!/usr/local/bin/php -q
<?php

// Redis configuration
$redisHost = 'redis';
$redisPort = 6379;
// Connect to Redis
$redis = new Redis();
$redis->connect($redisHost, $redisPort);

// Access command-line arguments using $argv
$scriptName = $argv[0];
$arg1 = isset($argv[1]) ? $argv[1] : null;
$arg2 = isset($argv[2]) ? $argv[2] : null;
$arg3 = isset($argv[3]) ? $argv[3] : null;
$arg4 = isset($argv[4]) ? $argv[4] : null;

// ---- Library -----------------------------------------------------------------------------------

function tcplogr_apikey_verify($redis, $apikey, $secret) {
	$apikey_data_json = $redis->hget('apikeys', $apikey);
	$apikey_data = json_decode($apikey_data_json, true);
	// echo "apikey_data: "; print_r($apikey_data); echo "\n\n";
	if (password_verify($secret, $apikey_data['secret'])) return $apikey_data;
	return false;
}

// ------------------------------------------------------------------------------------------------

function apikey_generate_new() {
    $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
    $key = '';
    for ($i = 0; $i < 24; $i++) {
        $key .= $characters[mt_rand(0, 61)]; // 0-61 covers all 62 characters
    }
    return $key;
}

function apikey_secret_new() { return substr(apikey_generate_new(), 0, 8); }

function setup_apikeys_init($redis) {
	$apikey_admin = apikey_generate_new();
	$apikey_secret = apikey_secret_new();
	$apikey_data = array ( 'admin' => true, 'secret' => password_hash($apikey_secret, PASSWORD_BCRYPT) );
	$apikey_data_json = json_encode ($apikey_data);
    $redis->hset('apikeys', $apikey_admin, $apikey_data_json);
	return array ($apikey_admin, $apikey_secret);
}

function setup_apikeys_new($redis) {
	$apikey_user = apikey_generate_new();
	$apikey_secret = apikey_secret_new();
	$apikey_data = array ( 'secret' => password_hash($apikey_secret, PASSWORD_BCRYPT) );
	$apikey_data['bucket'] = 'bucket_' . hash('fnv1a32', microtime());
	$apikey_data_json = json_encode ($apikey_data);
    $redis->hset('apikeys', $apikey_user, $apikey_data_json);
	return array ($apikey_user, $apikey_secret);
}

function apikeys_empty($redis) {
	// return empty($redis->hgetall('apikeys'));
	return ($redis->hlen('apikeys') === 0);
}

function apikeys_admin_find($redis) {
	$apikeys = $redis->hgetall('apikeys');
	foreach ($apikeys as $apikey => $apikey_data_json) {
		$apikey_data = json_decode($apikey_data_json, true);
		if ($apikey_data['admin'] === true) return array ( $apikey, $apikey_data);
	}
	return false;
}

function apikeys_admin_update($redis) {
	if ($apikey_find = apikeys_admin_find($redis)) {
		// echo "apikey_find: "; print_r($apikey_find);
		$apikey_admin = $apikey_find[0];
		$apikey_data = $apikey_find[1];
		$apikey_secret = apikey_secret_new();
		$apikey_data['secret'] = password_hash($apikey_secret, PASSWORD_BCRYPT);
		$apikey_data_json = json_encode ($apikey_data);
		$redis->hset('apikeys', $apikey_admin, $apikey_data_json);
	} else {
		echo "ERR: APIKEY for admin not found.\n\n";
	}
	return array ($apikey_admin, $apikey_secret);
}

function setup_apikeys_clear($redis) {
	$redis->del('apikeys');
}

switch ($arg1) {
	case 'apikeys':
		switch ($arg2) {
			case 'init':
				if (!apikeys_empty($redis)) {
					if ($arg3 == 'admin') {
						echo "ERR: APIKEYS not empty, will update secret\n\n";
						$apikey_info = apikeys_admin_update($redis);
						echo "APIKEY[admin]: {$apikey_info[0]} {$apikey_info[1]}\n";
						echo "IMPORTANT: SAVE THIS API KEY & SECRET\n\n";
					} else if ($arg3 == 'user') {
						echo "INFO: Will create new user\n\n";
						$apikey_info = setup_apikeys_new($redis);
						echo "APIKEY[admin]: {$apikey_info[0]} {$apikey_info[1]}\n";
						echo "IMPORTANT: SAVE THIS API KEY & SECRET\n\n";
					} else {
						echo "ERR: APIKEYS not empty, cannot init\n\n";
						break;
					}
				} else {
					$apikey_info = setup_apikeys_init($redis);
					echo "APIKEY[admin]: {$apikey_info[0]} {$apikey_info[1]}\n";
					echo "IMPORTANT: SAVE THIS API KEY & SECRET\n\n";
				}
			break;
			case 'list':
				echo "ERR/apikeys: WILL NOT IMPLEMENT apikeys list\n";
			break;
			case 'clear':
				setup_apikeys_clear($redis);
			break;
			case 'check':
				if ($apikey_data = tcplogr_apikey_verify($redis, $arg3, $arg4)) {
					echo "APIKEY verified\n";
					if ($apikey_data['admin'] === true) echo "  admin: true\n";
					if ($apikey_data['bucket']) echo "  bucket: {$apikey_data['bucket']}\n"; else echo "  bucket: none\n";
				} else {
					echo "APIKEY not verified\n";
				}
			break;
			default:
				echo "ERR/apikeys: wrong arg\n";
			break;
		}
	break;
	case 'help':
		echo "setup.php apikeys init [admin|user]\n";
		echo "setup.php apikeys list\n";
		echo "setup.php apikeys clear\n";
		echo "setup.php apikeys check APIKEYXXXXXXXXXXXX\n";
	break;
	default:
		echo "ERR/setup: wrong arg\n";
	break;
}

?>
