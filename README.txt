TCPLogr-Web

Components:
- Setup Utility: Initialize the system - database, etc.
- TCP Server: listens on a port for incoming connection and logs the received data.
- Web App; Web-based interface to view the logged data.
- API: REST/JSON API to access the data.

