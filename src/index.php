<?php
ob_start();

// Redis configuration
$redisHost = 'redis';
$redisPort = 6379;

// ---- Library -----------------------------------------------------------------------------------

function tcplogr_apikey_verify($redis, $apikey, $secret) {
	$apikey_data_json = $redis->hget('apikeys', $apikey);
	$apikey_data = json_decode($apikey_data_json, true);
	// echo "apikey_data: "; print_r($apikey_data); echo "\n\n";
	if (password_verify($secret, $apikey_data['secret'])) return $apikey_data;
	return false;
}

// ------------------------------------------------------------------------------------------------

// Connect to Redis
$redis = new Redis();
$redis->connect($redisHost, $redisPort);

session_start();

?>
<html>
    <head>
        <title>TCPLogr</title>
    </head>
    <body>
	<p align="center" style="background-color:#f8f8f8; padding:6px">
	<img src="tcplogr.png" align="left" />
<?php
echo '[ ';
$request_page = $_REQUEST['page'];
// if (!$request_page) $request_page = 'home';
// echo "request_page='$request'";
echo "<a href='/'>Home</a> ";	// MENU
if (!isset($_SESSION['verified'])) {
	echo "<a href='/?page=verify'>Verify</a> ";
} else {
	echo "<a href='/?page=logout'>Logout</a> ";
	if ($_SESSION['admin']) {
		echo " | Admin: ";
		echo "<a href='/?page=admin'>Admin</a> ";
	}
}
echo ' ] ';
if (isset($_SESSION['verified']) && !isset($_SESSION['admin'])) {
	echo "Bucket: <b>{$_SESSION['bucket']}</b>";
}
?>
	</p>
	<p>
<?php
switch ($request_page) {
	default:
	case 'home':
		if ($_SESSION['verified']) {
			if (!$_SESSION['admin']) {
				echo '<textarea id="log" name="log" rows="60" cols="160" readonly="true">';
				$values = $redis->lrange($_SESSION['bucket'], 0, -1);
				foreach ($values as $value) {
					echo "$value\n";
				}
				echo '</textarea>';
			} else {
				echo '<p style="background-color:#f8f8f8; padding:24">';
				echo 'Admin area';
				echo '</p>';
			}
		} else {
			echo 'Not verified';
		}
	break;

	case 'verify':	// Verify APIKEY
		$request_apikey = $_REQUEST['apikey'];
		$request_secret = $_REQUEST['secret'];
		if (!$request_apikey || !$request_secret) {
			echo '<form action="/" method="GET">';
			echo '<input type="hidden" name="page" value="verify" />';
			echo '<input type="text" name="apikey" value="' . $request_apikey . '" />';
			echo '<input type="text" name="secret" value="' . $request_secret . '" />';
			echo '<button type="submit" value="verify">Verify</button>';
			echo '</form>';
		} else {
			$apikey_data = tcplogr_apikey_verify($redis, $request_apikey, $request_secret);
			if ($apikey_data) {
				session_destroy();
				session_start();
				$_SESSION['verified'] = true;
				if ($apikey_data['admin']) {
					$_SESSION['admin'] = true;
					// NOTE: Admin does not have a Bucket
				} else {
					$_SESSION['bucket'] = $apikey_data['bucket'];
				}
				header('Location: /');
			} else {
				echo "Invalid APIKEY or SECRET";
			}
		}
	break;
	case 'logout':
		session_destroy();
		header('Location: /');
		exit;
	break;
}
?>
	</p>
    </body>
</html>
<?php ob_end_flush(); ?>