#!/usr/local/bin/php -q
<?php
error_reporting(E_ALL);
set_time_limit(0); /* Allow the script to hang around waiting for connections. */
ob_implicit_flush(); /* Turn on implicit output flushing so we see what we're getting as it comes in. */

// ------------------------------------------------------------------------------------------------

// Redis configuration
define('REDIS_HOST', 'redis');
define('REDIS_PORT', '6379');
// TCP Server configuration
// define('SERVER_ADDRESS', '127.0.0.1');
define('SERVER_ADDRESS', '0.0.0.0');
define('SERVER_PORT', '8999');

// ---- Library -----------------------------------------------------------------------------------

function tcplogr_apikey_verify($redis, $apikey, $secret) {
	$apikey_data_json = $redis->hget('apikeys', $apikey);
	$apikey_data = json_decode($apikey_data_json, true);
	// echo "apikey_data: "; print_r($apikey_data); echo "\n\n";
	if (password_verify($secret, $apikey_data['secret'])) return $apikey_data;
	return false;
}

// ------------------------------------------------------------------------------------------------

$MYPID; // Holds my PID.
$childPIDs = [];
$serverSocket;	// Server Socket.
$clientSocket;	// Client Socket.
$MYPID = getmypid();
echo "TCPLogr[P-$MYPID] started\n";
pcntl_async_signals(true);

// ------------------------------------------------------------------------------------------------

function signalHandler($signo, $siginfo) {
	global $serverSocket;	// Server Socket.
	global $clientSocket;	// Client Socket.
	global $childPIDs;
	echo "Received signal #$signo\n";
	// echo "siginfo " . print_r($siginfo) . "\n";
    switch ($signo) {
        case SIGTERM:
        case SIGINT:
            echo "Cleaning up...\n";
            echo "serverSocket[$serverSocket] clientSocket[$clientSocket] \n";
			if ($serverSocket) socket_close($serverSocket); // Close the server socket in the child process.
			if ($clientSocket) socket_close($clientSocket); // Close the client socket in the parent process.
			echo "PIDs: ";
			foreach ($childPIDs as $pid) {
				echo "$pid";
				posix_kill($pid, SIGTERM);
				echo "~x ";
			}
			echo "\n";
            exit(0);
        default:
            // Handle other signals if needed
            echo "This signal is not handled.\n";
            break;
    }
}

pcntl_signal(SIGTERM, 'signalHandler');
pcntl_signal(SIGINT, 'signalHandler');
pcntl_signal(SIGQUIT, 'signalHandler'); // For TESTING
// pcntl_signal(SIGCHLD, function ($signo) { $pid = pcntl_waitpid(-1, $status); echo "SIGCHLD: signo=$signo\n"; }); // NOTE: dos not work :(
pcntl_signal(SIGCHLD, SIG_IGN);	// Will ignore the child signal when its terminated , i.e. prevent from zombie child.

// Signals: https://www.php.net/manual/en/pcntl.constants.php
// ------------------------------------------------------------------------------------------------

// NOTE: Implemented with a non-blocking socket_accept.
// NOTE: While it is blocked, the process cannot handle signals with pcntl_signal handlers.
// Ref: socket_accept https://www.php.net/manual/en/function.socket-accept.php

if (($serverSocket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP)) === false) {
    echo "ERR/socket_create: " . socket_strerror(socket_last_error()) . "\n";
} else if (socket_bind($serverSocket, SERVER_ADDRESS, SERVER_PORT) === false) {
    echo "ERR/socket_bind: " . socket_strerror(socket_last_error($serverSocket)) . "\n";
} else if (socket_listen($serverSocket, 5) === false) {
    echo "socket_listen() failed: reason: " . socket_strerror(socket_last_error($serverSocket)) . "\n";
} else {
	// Set LINKGER option. Must be set HERE. Helps close the server socket immediately.
	$linger = array ('l_linger' => 0, 'l_onoff' => 1);
	socket_set_option($serverSocket, SOL_SOCKET, SO_LINGER, $linger);
	socket_set_nonblock($serverSocket);		// Set server socket as non-blocking.
	do { // MAIN LOOP: Accepting client connections
		
		if (($clientSocket = socket_accept($serverSocket)) === false) {
            usleep(100);
		} else if ($clientSocket == 0) {
			echo "ERR/socket_accept: " . socket_strerror(socket_last_error($serverSocket)) . "\n";
			// break; // QUIT the main loop and the app.
		} else {
			// Fork a child process to handle the connection
			if (($pid = pcntl_fork()) == -1) {	// ERROR: Forking failed
				echo "ERR/pcntl_fork: " . socket_strerror(socket_last_error($serverSocket)) . "\n";
				break; // QUIT the main loop and the app.
			} else if ($pid != 0) {	// This is the PARENT process
				global $childPIDs;
				$childPIDs[] = $pid;
				socket_close($clientSocket); // Close the client socket in the parent process
				$clientSocket = NULL;	// Invalidate the value.
				echo "TCPLogr/forked:$pid\n";
				continue;	// Continue to listen for new connections.
			} else {	// pid=0 ==> This is the CHILD process
				global $childPIDs;
				$childPIDs = NULL; // Clear the list of PIDs.
				socket_close($serverSocket); // Close the server socket in the child process.
				$serverSocket = NULL;	// Invalidate the value.
				$MYPID = getmypid();
				echo "TCPLogr[P-$MYPID] forked\n";
				clientConnectionHandler($clientSocket);	// Handle the connection in the child process
				echo "TCPLogr[P-$MYPID] concluded\n";
				socket_close($clientSocket);	// Close the client socket
				break; 							// ... and break, to exit the child process
			}
		}
	} while (true);
}

exit();

// ------------------------------------------------------------------------------------------------

// NOTE: When socket_read blocks, the signal handling also blocks.
//       SHOULD implement non-blocking socket functions. See TODO file.

function clientConnectionHandler($clientSocket) {
	global $MYPID;
	$field_names = array();	// Clear the filed names
	socket_getpeername($clientSocket, $clientAddress, $clientPort);
    socket_write($clientSocket, "HELLO: [$clientAddress:$clientPort]\n");	// Send Welcome.
	echo "TCPLogr[P-$MYPID] HELLO [$clientAddress:$clientPort]\n";
	// Connect to Redis
	$redis = new Redis();
	$redis->connect(REDIS_HOST, REDIS_PORT);
	$apikey_verified = false;
    do {
		if (false === ($buf = @socket_read($clientSocket, 2048, PHP_NORMAL_READ))) {	// "@" to suppress the warning on client-disconnect.
			echo "TCPLogr[P-$MYPID]ERR: socket_read() failed: reason: " . socket_strerror(socket_last_error($clientSocket)) . "\n";
			return;
		}
		if (!$buf = trim($buf)) continue;
		echo "TCPLogr[P-$MYPID]RCV: $buf\n";
        socket_write($clientSocket, "RCV: $buf\n");
		
		if ($buf[0] == '#') {
			if ($buf[1] == ' ') { /* Comment - nothing to do */
			} else if ($buf[1] == '#') { // Command
				$cmd_line = substr($buf, 2);
				$cmd_parts = explode(":", $cmd_line);
				$cmd_code = $cmd_parts[0];
				switch ($cmd_code) {
					case 'HELO':
						socket_getpeername($clientSocket, $clientAddress, $clientPort);
						socket_write($clientSocket, "HELLO [$clientAddress:$clientPort]\n");
					break;
					case 'AUTH':
						$cmd_auth = explode(",", $cmd_parts[1]);
						$auth_apikey = $cmd_auth[0];
						$auth_secret = $cmd_auth[1];
						echo "AUTH: apikey=$auth_apikey secret=$auth_secret \n";
						$apikey_data = tcplogr_apikey_verify($redis, $auth_apikey, $auth_secret);
						if ($apikey_data) {
							$apikey_verified = true;
							$bucket = $apikey_data['bucket'];
							echo "AUTH: verified, bucket=$bucket\n";
						} else {
							echo "AUTH: failed\n";
						}
						$field_names = array();	// Clear the filed names
					break;
					case 'FN':	// Field names
						$field_names = str_getcsv(substr($buf, 5));
						// var_dump($field_names);
					break;
					case 'QUIT':	// Invalidate
						socket_write($clientSocket, "QUIT\n");
						$apikey_verified = false;
						$field_names = array();	// Clear the filed names
					break;
					case 'EXIT':	// Exit
						socket_write($clientSocket, "EXIT\n");
						return;
					break;
					default:
						echo "ERR: '$cmd_code' - Unknown command\n";
					break;
				}
			}
		} else {
			// var_dump(json_encode(str_getcsv($buf)));
			if ($apikey_verified) {	// Check if authenticated
				$log_data = [ 'timestamp' => date('Y-m-d H:i:s') ];
				$buf_data = str_getcsv($buf);
				$field_data = array();
				if ($field_names) {
					$index = 0;
					foreach ($buf_data as $buf_data_value) {
						if (isset($field_names[$index])) {
							$field_name = $field_names[$index];
						} else {
							$field_name = '?' . $index;
						}
						$field_data[$field_name] = $buf_data_value;
						$index++;
					}
				} else {
					$field_data = $buf_data;
				}					
				var_dump(json_encode($field_data));
				$log_data = array_merge($log_data, $field_data);
				// var_dump(json_encode($log_data));
				$redis->rpush($bucket, json_encode($log_data));
			}
		}
    } while (true);
}

?>
