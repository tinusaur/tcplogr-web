#!/bin/sh

# Start application in the background
/srv/tcplogr/serv/tcplogr-serv &

# Start PHP-FPM
php-fpm

# Keep the script running or perform other background tasks if needed
# tail -f /dev/null

